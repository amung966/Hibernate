package com.hibernate.model;
// Generated Aug 17, 2017 7:38:32 PM by Hibernate Tools 3.4.0.CR1

/**
 * MessageViewId generated by hbm2java
 */
public class MessageViewIds implements java.io.Serializable {

	private String [] TId;
	private int [] TAId;
	private String [] TInfo;
	private String [] TTitle;
	private String [] TCreated;
	private int [] TChanged;
	private int [] TLasttime;
	private String [] AId;
	private String [] account;
	private String [] passwd;
	private String [] name;
	private int [] ACreated;
	private int [] AChanged;
	private int [] ALasttime;

	public MessageViewIds() {
	}

	public MessageViewIds(String [] TId, int [] TAId, String [] TInfo, String [] TTitle, String [] TCreated, int [] TChanged, int [] TLasttime,
			String [] AId, String [] account, String [] passwd, String [] name, int [] ACreated, int [] AChanged, int [] ALasttime) {
		this.TId = TId;
		this.TAId = TAId;
		this.TInfo = TInfo;
		this.TTitle = TTitle;
		this.TCreated = TCreated;
		this.TChanged = TChanged;
		this.TLasttime = TLasttime;
		this.AId = AId;
		this.account = account;
		this.passwd = passwd;
		this.name = name;
		this.ACreated = ACreated;
		this.AChanged = AChanged;
		this.ALasttime = ALasttime;
	}

	public String[] getTId() {
		return this.TId;
	}

	public void setTId(String [] TId) {
		this.TId = TId;
	}

	public int[] getTAId() {
		return this.TAId;
	}

	public void setTAId(int [] TAId) {
		this.TAId = TAId;
	}

	public String[] getTInfo() {
		return this.TInfo;
	}

	public void setTInfo(String [] TInfo) {
		this.TInfo = TInfo;
	}

	public String[] getTTitle() {
		return this.TTitle;
	}

	public void setTTitle(String [] TTitle) {
		this.TTitle = TTitle;
	}

	public String[] getTCreated() {
		return this.TCreated;
	}

	public void setTCreated(String [] TCreated) {
		this.TCreated = TCreated;
	}

	public int[] getTChanged() {
		return this.TChanged;
	}

	public void setTChanged(int [] TChanged) {
		this.TChanged = TChanged;
	}

	public int[] getTLasttime() {
		return this.TLasttime;
	}

	public void setTLasttime(int [] TLasttime) {
		this.TLasttime = TLasttime;
	}

	public String[] getAId() {
		return this.AId;
	}

	public void setAId(String [] AId) {
		this.AId = AId;
	}

	public String[] getAccount() {
		return this.account;
	}

	public void setAccount(String [] account) {
		this.account = account;
	}

	public String[] getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String [] passwd) {
		this.passwd = passwd;
	}

	public String[] getName() {
		return this.name;
	}

	public void setName(String [] name) {
		this.name = name;
	}

	public int[] getACreated() {
		return this.ACreated;
	}

	public void setACreated(int [] ACreated) {
		this.ACreated = ACreated;
	}

	public int[] getAChanged() {
		return this.AChanged;
	}

	public void setAChanged(int [] AChanged) {
		this.AChanged = AChanged;
	}

	public int[] getALasttime() {
		return this.ALasttime;
	}

	public void setALasttime(int [] ALasttime) {
		this.ALasttime = ALasttime;
	}


}
