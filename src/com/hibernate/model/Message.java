package com.hibernate.model;
// Generated Aug 17, 2017 7:38:32 PM by Hibernate Tools 3.4.0.CR1

/**
 * Message generated by hbm2java
 */
public class Message implements java.io.Serializable {

	private Integer TId;
	private int TAId;
	private String TInfo;
	private String TTitle;
	private int TCreated;
	private int TChanged;
	private int TLasttime;

	public Message() {
	}

	public Message(int TAId, String TInfo, String TTitle, int TCreated, int TChanged, int TLasttime) {
		this.TAId = TAId;
		this.TInfo = TInfo;
		this.TTitle = TTitle;
		this.TCreated = TCreated;
		this.TChanged = TChanged;
		this.TLasttime = TLasttime;
	}

	public Integer getTId() {
		return this.TId;
	}

	public void setTId(Integer TId) {
		this.TId = TId;
	}

	public int getTAId() {
		return this.TAId;
	}

	public void setTAId(int TAId) {
		this.TAId = TAId;
	}

	public String getTInfo() {
		return this.TInfo;
	}

	public void setTInfo(String TInfo) {
		this.TInfo = TInfo;
	}

	public String getTTitle() {
		return this.TTitle;
	}

	public void setTTitle(String TTitle) {
		this.TTitle = TTitle;
	}

	public int getTCreated() {
		return this.TCreated;
	}

	public void setTCreated(int TCreated) {
		this.TCreated = TCreated;
	}

	public int getTChanged() {
		return this.TChanged;
	}

	public void setTChanged(int TChanged) {
		this.TChanged = TChanged;
	}

	public int getTLasttime() {
		return this.TLasttime;
	}

	public void setTLasttime(int TLasttime) {
		this.TLasttime = TLasttime;
	}

}
