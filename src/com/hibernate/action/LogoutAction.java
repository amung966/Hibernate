package com.hibernate.action;

import java.util.Map;

import com.hibernate.model.Account;
import com.hibernate.server.UserService;
import com.opensymphony.xwork2.ActionContext;

public class LogoutAction {
	private String uid;
	ActionContext ac = ActionContext.getContext();
	Map session = ac.getSession();
	
	public void setUid(String uid) {
        this.uid = uid;
    }
	
	public String getUid() {
        return uid;
    }
	
	public String execute() throws Exception{
		
		session.clear();
		
		return "success";
		
	}
	
}
