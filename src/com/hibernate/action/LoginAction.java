package com.hibernate.action;

import java.util.Map;

import com.hibernate.model.Account;
import com.hibernate.server.MyClass;
import com.hibernate.server.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport{
	private String account , passwd;
	MyClass my = new MyClass();
	ActionContext ac = ActionContext.getContext();
	Map session = ac.getSession();
	
	public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    
	public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    
    public String execute() throws Exception{
    	
    		System.out.println(account);
		System.out.println(passwd);
		
		Account acc = new Account();
		UserService dbuser = new UserService();
		
		acc = dbuser.hasUser(account, my.MD5(passwd));
		if(acc.getAccount() != null) {
			session.put("logined", "login");
			session.put("uid" , acc.getAId());
			session.put("name" , acc.getName());
			return "success";
		}else {
			session.put("logined", null);
			return "error";
		}
		
    }
}
