package com.hibernate.server;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.google.common.io.Files;
import com.hibernate.model.Account;
import com.hibernate.model.File;
import com.hibernate.model.Message;
import com.hibernate.model.MessageView;
import com.hibernate.model.MessageViewId;
import com.hibernate.model.MessageViewIds;

public class MessageService {
	
	private Session session;
	private Transaction tx;
	private Query query;
	private List list;
	private Message message;
	private MessageViewIds messages;
	private com.hibernate.model.Files filess;
	private com.hibernate.model.File files;
	private MyClass my;
	private Long tss;
	private String ts;
	
	public MessageService(){
		list = null;
		query = null;
		session = HibernateUtil.getSessionFactory().openSession();
		message = new Message();
		messages = new MessageViewIds();
		files = new com.hibernate.model.File();
		filess = new com.hibernate.model.Files();
		tss = System.currentTimeMillis()/1000;
		ts = Long.toString(tss);
		my = new MyClass();
	}
	public MessageViewIds getMsg() {
		String [] uname , date , mesg , title ,aid , tid;
		
		String sql = "from MessageView";
		Query query = session.createQuery(sql);
		
		Iterator message = query.list().iterator();
		int cou = query.list().size();
		uname = new String [cou];
		date = new String [cou];
		tid = new String [cou];
		mesg = new String [cou];
		aid = new String[cou];
		title = new String [cou];
		cou = 0;
		while(message.hasNext()) {
			MessageView msgv = (MessageView) message.next();
			MessageViewId m = new MessageViewId();
			m = msgv.getId();
			uname [cou] = m.getName();
			date [cou] = my.Date(m.getTCreated());
			tid [cou] = m.getTId()+"";
			mesg [cou] = m.getTInfo();
			aid [cou] = m.getAId()+"";
			title [cou] = m.getTTitle();
			cou = cou +1;
        }
		
		messages.setAId(aid);
		messages.setTId(tid);
		messages.setTCreated(date);
		messages.setName(uname);
		messages.setTInfo(mesg);
		messages.setTTitle(title);
		
		return messages;
	}
	public com.hibernate.model.Files getFile() {
		String [] fid ,ftid , file , ofile;
		String sql = "from File";
		Query query = session.createQuery(sql);
		com.hibernate.model.File f = new com.hibernate.model.File();
		Iterator iterator =  query.list().iterator();
		
		int cou = query.list().size();
		fid = new String[cou];
		ftid = new String[cou];
		file = new String[cou];
		ofile = new String[cou];
		
		cou = 0;
		while(iterator.hasNext()) {
			f = (com.hibernate.model.File) iterator.next();
			fid[cou] = f.getFId()+"";
			file[cou] = f.getFName();
			ofile[cou] = f.getFOName();
			ftid[cou] = f.getFTId()+"";
			cou = cou + 1;
		}
		
		filess.setFId(fid);
		filess.setFTId(ftid);
		filess.setFName(file);
		filess.setFOName(ofile);
		
		return filess;
	}
	
	public boolean insertMsg(String user , String msg , String title , String file[] , String ofile[]) {
		tx = session.beginTransaction();
		
		message.setTAId(Integer.parseInt(user));
		message.setTInfo(msg);
		message.setTTitle(title);
		message.setTCreated(Integer.parseInt(ts));
		message.setTChanged(Integer.parseInt(ts));
		message.setTLasttime(Integer.parseInt(ts));
		
		session.save(message);
		
		if(file != null) {
			insertFile(message.getTId() , file ,  ofile);
		}
		
		tx.commit();
		return true;
	}
	
	public boolean delMsg(String tid) {
		tx = session.beginTransaction();
		
		message = (Message) session.get(Message.class, Integer.parseInt(tid));
		tx.commit();
		
		String sql = "from File where FTId ="+tid;
		Query query = session.createQuery(sql);
		com.hibernate.model.File f = new com.hibernate.model.File();
		Iterator iterator =  query.list().iterator();
		int cou = query.list().size();
		cou = 0;
		while(iterator.hasNext()) {
			f = (com.hibernate.model.File) iterator.next();
			delFile(f.getFId()+"");
			String target = ServletActionContext.getServletContext().getRealPath("/upload/"+f.getFName());
			my.deleteFile(target);
			cou = cou + 1;
		}
		
		tx = session.beginTransaction();
		session.delete(message);
		tx.commit();
		
		return true;
	}
	
	public boolean updMsg(String tid , String msg , String title , String file[] , String ofile[]) {
		tx = session.beginTransaction();
		message = (Message) session.get(Message.class, Integer.parseInt(tid));
		tx.commit();
		
		tx = session.beginTransaction();
		
		message.setTInfo(msg);
		message.setTTitle(title);
		message.setTChanged(Integer.parseInt(ts));
		message.setTChanged(Integer.parseInt(ts));
		
		if(file != null) {
			insertFile(Integer.parseInt(tid) , file , ofile);
		}
		
		session.update(message);
		tx.commit();
		
		return true;
	}
	
	public boolean delFile(String fid) {
		

		tx = session.beginTransaction();
		files = (com.hibernate.model.File) session.get(com.hibernate.model.File.class, Integer.parseInt(fid));
		
		session.delete(files);
		tx.commit();
		
		return true;
	}
	
	private void insertFile(int tid , String file[] , String ofile[]) {
		
		for(int i=0; i<file.length; i++) {
			files = new com.hibernate.model.File();
			
			files.setFTId(tid);
			files.setFName(file[i]);
			files.setFOName(ofile[i]);
			files.setFCreated(Integer.parseInt(ts));
			files.setFChanged(Integer.parseInt(ts));
			files.setFLasttime(Integer.parseInt(ts));
			
			session.save(files);

		}
	}
}
